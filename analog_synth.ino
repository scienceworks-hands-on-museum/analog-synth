
/**
 * Hardware controller for mapping physical patch cords from source to destination.
 * Patch cords are fixed on one end and can be plugged into multiple inputs.
 * A signal is sent through the patch cord to be received by the Arduino.
 * By reading the signal, the Arduino determines which cord is plugged where.
 * This information is communicated over USB serial to the PC for further use.
 */

#define FIRST_IN_PIN 22
#define LAST_IN_PIN 53
#define FIRST_OUT_PIN 2
#define LAST_OUT_PIN 13
#define NUM_OUT_PINS (LAST_OUT_PIN - FIRST_OUT_PIN + 1)
#define PULSE_INTERVAL_MS 50
#define FULL_PULSE_WIDTH NUM_OUT_PINS * PULSE_INTERVAL_MS
#define PULSE_TIMEOUT FULL_PULSE_WIDTH * 20 + 50

char mapping[200];

void setup() {

  for (int i = FIRST_OUT_PIN; i <= LAST_OUT_PIN; i++) {
    pinMode(i, OUTPUT);
    digitalWrite(i, LOW);
  }

  for (int i = FIRST_IN_PIN; i <= LAST_IN_PIN; i++) {
    pinMode(i, INPUT);
  }

  Serial.begin(9600);
}

void loop() {
  strcpy(mapping, "");

  bool empty = true;
  unsigned int i, j;

  for (i = FIRST_OUT_PIN; i <= LAST_OUT_PIN; i++) {
    digitalWrite(i, HIGH);

    for (j = FIRST_IN_PIN; j <= LAST_IN_PIN; j++) {
      if (digitalRead(j) == HIGH) {

        if (!empty) {
          strcat(mapping, ",");
        }

        sprintf(mapping, "%s%u:%u", mapping, i, j);
        empty = false;
        
        break;
      }
    }

    digitalWrite(i, LOW);
  }

  Serial.println(mapping);
}
